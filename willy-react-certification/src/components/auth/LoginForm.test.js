import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import store from "../../redux/store/store";
import { Provider } from "react-redux";
import { screen } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";

import LoginForm, { validatePassword, validateEmail } from "./LoginForm";

let container = null;
const loading = false;
const handleLoginAuth = jest.fn();

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});
let testId = "";

describe("password Toggle Show and input", () => {
  it("password is emtpy, password toggle should be disabled  ", () => {
    testId = "logingform-submitbutton-showpassword";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const button = screen.getByTestId(testId);

    //Assert
    expect(button).toHaveAttribute("disabled");
  });

  it("Enter password, then remove password. password toggle should be disabled  ", () => {
    testId = "logingform-submitbutton-showpassword";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const button = screen.getByTestId(testId);
    testId = "logingform-inputfield-password";
    const password = "Ttest123";
    const passwordInputField = screen.getByTestId(testId);

    fireEvent.change(passwordInputField, { target: { value: password } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    expect(button).not.toHaveAttribute("disabled");

    fireEvent.change(passwordInputField, { target: { value: "" } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    expect(button).toHaveAttribute("disabled");
  });

  it("with password, button is enabled and input field type should be text", () => {
    testId = "logingform-submitbutton-showpassword";

    //Arrange
    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });

    const button = screen.getByTestId(testId);
    const passwordInputField = screen.getByTestId(
      "logingform-inputfield-password"
    );
    //Act
    fireEvent.change(passwordInputField, { target: { value: "23" } });

    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    //Assert
    expect(button).not.toHaveAttribute("disabled");
    expect(passwordInputField).toHaveAttribute("type", "text");
    expect(passwordInputField.type).toBe("text");
  });

  it("with password,show password to hidden again", () => {
    testId = "logingform-submitbutton-showpassword";

    //Arrange
    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });

    const button = screen.getByTestId(testId);
    const passwordInputField = screen.getByTestId(
      "logingform-inputfield-password"
    );

    //Act
    fireEvent.change(passwordInputField, { target: { value: "23" } });

    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    //Assert
    expect(button).not.toHaveAttribute("disabled");
    expect(passwordInputField).toHaveAttribute("type", "text");
    expect(passwordInputField.type).toBe("text");

    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );
    expect(passwordInputField).toHaveAttribute("type", "password");
  });

  it("Password input field type should be Password", () => {
    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });

    const button = screen.getByTestId(testId);
    const passwordInputField = screen.getByTestId(
      "logingform-inputfield-password"
    );

    fireEvent.change(passwordInputField, { target: { value: "23" } });

    //Assert
    expect(button).not.toHaveAttribute("disabled");
    expect(passwordInputField.type).toBe("password");
  });
});

describe("Login validation", () => {
  it("user Name is empty, Login button  should be disabled", () => {
    testId = "logingform-submitbutton-login";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const button = screen.getByTestId(testId);

    //Assert
    expect(button).toHaveAttribute("disabled");
  });

  it("password is empty, Login button should be disabled", () => {
    testId = "logingform-submitbutton-login";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const logingBbutton = screen.getByTestId(testId);
    const emailInputField = screen.getByTestId("logingform-inputfield-email");

    fireEvent.change(emailInputField, { target: { value: "test@test.co" } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });

    //Assert

    expect(logingBbutton).toHaveAttribute("disabled");
  });

  it("valid login, Login button should be enabled", () => {
    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });

    const logingBbutton = screen.getByTestId(testId);
    const emailInputField = screen.getByTestId("logingform-inputfield-email");
    const passwordInputField = screen.getByTestId(
      "logingform-inputfield-password"
    );

    fireEvent.change(emailInputField, { target: { value: "test@test.co" } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });
    fireEvent.change(passwordInputField, { target: { value: "2wEfffF" } });
    fireEvent.change(passwordInputField, { target: { value: "2wEfffFe" } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    //Assert

    expect(logingBbutton).not.toHaveAttribute("disabled");
  });
});

describe("email validation", () => {
  it("with valid email, state.emailError with is false", () => {
    testId = "logingform-inputfield-email";
    const email = "test@test.co";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const emailInputField = screen.getByTestId(testId);

    fireEvent.change(emailInputField, { target: { value: email } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });

    expect(container).not.toHaveTextContent("Invalid email format");
  });

  it("invalid email to valid email to invalid email,trackin down the email message got removed and put back", () => {
    testId = "logingform-inputfield-email";
    const email = "test@test.co";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const emailInputField = screen.getByTestId(testId);

    fireEvent.change(emailInputField, { target: { value: "ss@ttes." } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });

    expect(container).toHaveTextContent("Invalid email format");

    fireEvent.change(emailInputField, { target: { value: email } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });

    expect(container).not.toHaveTextContent("Invalid email format");

    fireEvent.change(emailInputField, { target: { value: "ss@ttes." } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });

    expect(container).toHaveTextContent("Invalid email format");
  });

  it("with invalid email, state emailError", () => {
    testId = "logingform-inputfield-email";
    const email = "test#t.co";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const emailInputField = screen.getByTestId(testId);

    fireEvent.change(emailInputField, { target: { value: email } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });

    expect(container).toHaveTextContent("Invalid email format");
  });
});

describe("password Validation", () => {
  it("valid Password, no password error", () => {
    testId = "logingform-inputfield-password";
    const password = "Ttest123";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const passwordInputField = screen.getByTestId(testId);

    fireEvent.change(passwordInputField, { target: { value: password } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    expect(container).not.toHaveTextContent(
      "Upper case character is missingLower case character is missingDigit character is missingPassword must have 6 to 10 characters"
    );
  });

  it("TEsting ERror MessageShowing and removed", () => {
    testId = "logingform-inputfield-password";
    const password = "Ttest123";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const passwordInputField = screen.getByTestId(testId);

    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });
    expect(container).not.toHaveTextContent(
      "Upper case character is missingDigit character is missingPassword must have 6 to 10 characters"
    );

    fireEvent.change(passwordInputField, { target: { value: "1Rm" } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });
    expect(container).not.toHaveTextContent(
      "Lower case character is missingPassword must have 6 to 10 characters"
    );

    fireEvent.change(passwordInputField, { target: { value: "1R" } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });
    expect(container).not.toHaveTextContent(
      "Upper case character is missingPassword must have 6 to 10 characters"
    );

    fireEvent.change(passwordInputField, { target: { value: password } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    expect(container).not.toHaveTextContent(
      "Upper case character is missingLower case character is missingDigit character is missingPassword must have 6 to 10 characters"
    );
  });

  it("missing upper case letter in password, expect passwordError", () => {
    testId = "logingform-inputfield-password";
    const password = "ttest123";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const passwordInputField = screen.getByTestId(testId);

    fireEvent.change(passwordInputField, { target: { value: password } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    expect(container).toHaveTextContent("Upper case character is missing");
  });

  it("missing lower case letter in password, expect passwordError", () => {
    testId = "logingform-inputfield-password";
    const password = "TTEST123";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const passwordInputField = screen.getByTestId(testId);

    fireEvent.change(passwordInputField, { target: { value: password } });
    fireEvent.keyDown(passwordInputField, { key: "T", code: "KeyT" });

    expect(container).toHaveTextContent("Lower case character is missing");
  });

  it("missing digit in password,, expect passwordError", () => {
    testId = "logingform-inputfield-password";
    const password = "Ttestrff";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const passwordInputField = screen.getByTestId(testId);

    fireEvent.change(passwordInputField, { target: { value: password } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    expect(container).toHaveTextContent("Digit character is missing");
  });

  it("password is less then 6 characters, expect passwordError", () => {
    testId = "logingform-inputfield-password";
    const password = "Test1";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const passwordInputField = screen.getByTestId(testId);

    fireEvent.change(passwordInputField, { target: { value: password } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    expect(container).toHaveTextContent(
      "Password must have 6 to 10 characters"
    );
  });

  it("password is more then 10 characters, expect passwordError", () => {
    testId = "logingform-inputfield-password";
    const password = "1Er12345678910";

    act(() => {
      render(
        <Provider store={store}>
          <LoginForm handleLoginAuth={handleLoginAuth} loading={loading} />
        </Provider>,
        container
      );
    });
    const passwordInputField = screen.getByTestId(testId);

    fireEvent.change(passwordInputField, { target: { value: password } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    expect(container).toHaveTextContent(
      "Password must have 6 to 10 characters"
    );
  });
});
