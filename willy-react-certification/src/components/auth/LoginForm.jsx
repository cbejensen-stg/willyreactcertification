import React, { Component } from "react";
import InputField from "../componentfields/InputField";
import SubmitButton from "../componentfields/SubmitButton";
import { connect } from "react-redux";
/*
#Lifecycle methods
 - constructor
  - Render
*/
class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: [],
      emailError: false,

      loginSubmitButtonIsDisabled: true,

      passwordButtonText: "Show Password",
      passwordInputFieldType: "password",
      showPasswordbuttonIsDisabled: true,
      passwordError: false,
    };

    this.handleLoginClick = this.handleLoginClick.bind(this);
  }

  setInputValue(property, value) {
    value = value.trim();

    this.setState({
      [property]: value,
    });

    if (property === "email" && value.length > 0) {
      this.validateEmail(value);
    }

    if (property === "password") {
      if (value.length > 0) {
        this.validatePassword(value);
      } else {
        if (!this.state.showPasswordbuttonIsDisabled) {
          this.setState({
            showPasswordbuttonIsDisabled: true,
            passwordError: true,
            errors: [],
          });
        }
      }
    }

    if (
      this.state.password.length >= 6 &&
      this.state.email.length > 0 &&
      !this.state.passwordError &&
      !this.state.emailError
    ) {
      this.setState({
        loginSubmitButtonIsDisabled: false,
        errors: [],
      });
    } else {
      this.setState({
        loginSubmitButtonIsDisabled: true,
      });
    }
  }

  handleLoginClick() {
    let result = {};

    result = {
      data: {
        email: this.state.email,
        password: this.state.password,
      },
      bodyRequest: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      }),
    };
    this.props.handleLoginAuth(result);
  }

  toggleShowPassword() {
    let temp = this.state.passwordInputFieldType;
    const tempDisplay = temp === "password" ? "Hide Password" : "Show password";

    if (temp === "password") temp = "text";
    else temp = "password";

    this.setState({
      passwordButtonText: tempDisplay,
      passwordInputFieldType: temp,
    });

    if (temp === "text") {
    }
  }

  validateEmail(email) {
    const pattern = /[a-zA-Z0-9]+[.]?([a-zA-Z0-9]+)?[@][a-z]{3,9}[.][a-z]{2,5}/g;
    const result = pattern.test(email);
    if (result) {
      this.setState({
        emailError: false,
        errors: [],
      });
    } else {
      const errorMessage = "Invalid email format";
      this.setState({
        emailError: true,
      });

      if (this.state.errors.length < 1) {
        const temp = [...this.state.errors, errorMessage];
        this.setState({
          emailError: true,
          errors: temp,
        });
      }
    }
  }

  validatePassword(password) {
    const oneCapitalRegex = /[A-Z]+/;
    const oneLowerRegex = /[a-z]+/;
    const oneDegitsRegex = /\d+/;

    const hasCapitalLetter = oneCapitalRegex.test(password);
    const hasLowerLetter = oneLowerRegex.test(password);
    const hasDegits = oneDegitsRegex.test(password);

    const sixToTen = password.length >= 6 && password.length <= 10;

    const showPasswordbuttonIsDisabled =
      password.length < 1 || password === null;

    const isValidePassword =
      hasCapitalLetter && hasLowerLetter && hasDegits && sixToTen;
    let errorMessage = [...this.state.errors];

    if (!hasCapitalLetter) {
      this.addErrorIfNotExists(errorMessage, "Upper case character is missing");
    } else {
      this.removeErrorIfExists(errorMessage, "Upper case character is missing");
    }
    if (!hasLowerLetter) {
      this.addErrorIfNotExists(errorMessage, "Lower case character is missing");
    } else {
      this.removeErrorIfExists(errorMessage, "Lower case character is missing");
    }

    if (!hasDegits) {
      this.addErrorIfNotExists(errorMessage, "Digit character is missing");
    } else {
      this.removeErrorIfExists(errorMessage, "Digit character is missing");
    }

    if (!sixToTen) {
      this.addErrorIfNotExists(
        errorMessage,
        "Password must have 6 to 10 characters"
      );
    } else {
      this.removeErrorIfExists(
        errorMessage,
        "Password must have 6 to 10 characters"
      );
    }
    const tempError = [...errorMessage];

    this.setState({
      showPasswordbuttonIsDisabled: showPasswordbuttonIsDisabled,
      passwordError: !isValidePassword,
      errors: tempError,
    });
    return isValidePassword;
  }

  addErrorIfNotExists(myArray = [], message) {
    let test = this.state.errors.some((x) => {
      return x === message;
    });
    if (!test) {
      myArray.push(message);
    }
    return myArray;
  }

  removeErrorIfExists(myArray) {
    var what,
      a = arguments,
      L = a.length,
      ax;
    while (L > 1 && myArray.length) {
      what = a[--L];
      while ((ax = myArray.indexOf(what)) !== -1) {
        myArray.splice(ax, 1);
      }
    }
    return myArray;
  }

  render() {
    let keyId = 1;
    /*
    #Array Function - map
    map the array items into html code
  */
    let errors = this.state.errors.map((item) => {
      return <p key={++keyId}>{item}</p>;
    });

    return (
      <div>
        <div className="loginForm">
          Log In
          <InputField
            data_testid="logingform-inputfield-email"
            type="text"
            placeholder="Email"
            value={this.state.email ? this.state.email : ""}
            onChange={(value) => this.setInputValue("email", value)}
          />
          <div className="loginform-password_with-button">
            <InputField
              data_testid="logingform-inputfield-password"
              inputclassName="loginform-input-with-button"
              type={this.state.passwordInputFieldType}
              placeholder="Password"
              value={this.state.password ? this.state.password : ""}
              onChange={(value) => this.setInputValue("password", value)}
            />
            <SubmitButton
              data_testid="logingform-submitbutton-showpassword"
              text={this.state.passwordButtonText}
              buttonclassName="logingform-btn-show-hide-password"
              disabled={this.state.showPasswordbuttonIsDisabled}
              onClick={() => this.toggleShowPassword()}
            />
          </div>
          <SubmitButton
            data_testid="logingform-submitbutton-login"
            text="Login"
            disabled={this.state.loginSubmitButtonIsDisabled}
            onClick={(event) => this.handleLoginClick(event)}
          />
        </div>
        <div data_testid="logingform-submitbutton-error">{errors}</div>
      </div>
    );
  }
}

export default connect()(LoginForm);
