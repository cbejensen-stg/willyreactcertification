import React from "react";
import { unmountComponentAtNode } from "react-dom";
import { render } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { screen, getByTestId, waitForElement } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";

import InputField from "./InputField";

const inputclassName = "search-input";
const type = "text";
const placeholder = "Search...";
const value = "value 123";
const testId = "inputField-input";
const onChange = jest.fn();

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const setup = () => {
  const utils = render(
    <InputField
      inputclassName={inputclassName}
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />,
    container
  );
  let myElement = screen.getByTestId(testId);
  return { utils, ...myElement };
};

it("renders InputField", async () => {
  const { myElement } = setup();

  const expected = "12352";

  const input = screen.getByTestId(testId);
  fireEvent.change(input, {
    target: { value: expected },
  });

  expect(input.value).toBe(value);
});
