import React from "react";
import { Link } from "react-router-dom";
import expect, { createSpy } from "expect";
import { shallow, mount, render } from "enzyme";
import NavBar from "./navbar";

describe("Navigation Testing", () => {
  const wrapper = shallow(<NavBar />);
  const navStyle = {
    color: "white",
  };

  it("Should have nav Home", () => {
    const expectedElement = (
      <Link style={navStyle} to="/">
        <li>Home</li>
      </Link>
    );

    expect(wrapper.containsMatchingElement(expectedElement)).toBe(true);

    expect(wrapper.find("nav").length).toEqual(1);
  });

  it("Should have nav Categories", () => {
    const expectedElement = (
      <Link style={navStyle} to="/categories">
        <li>Categories</li>
      </Link>
    );

    expect(wrapper.containsMatchingElement(expectedElement)).toBe(true);
  });

  it("Should have nav Search", () => {
    const expectedElement = (
      <Link style={navStyle} to="/search">
        <li>Search</li>
      </Link>
    );

    expect(wrapper.containsMatchingElement(expectedElement)).toBe(true);
  });

  it("Should have nav Jokes", () => {
    const expectedElement = (
      <Link style={navStyle} to="/jokes">
        <li>Jokes</li>
      </Link>
    );

    expect(wrapper.containsMatchingElement(expectedElement)).toBe(true);
  });
});
