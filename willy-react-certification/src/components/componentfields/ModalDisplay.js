import React, { Component } from "react";

import Modal from "react-modal";

/*
#Styles 
we Can add Style to specific component if wanted to

*/
import "./styles.css";
/*
  #class Component
 there is only 2 ways to create a component we either use Class Component or Function Component
*/
class ModalDisplay extends Component {
  constructor(props) {
    super(props);

    this.onClose = this.onClose.bind(this);
  }

  onClose = (e) => {
    this.props.onClose && this.props.onClose(e);
    this.props.onRequestClose();
  };
  render() {
    if (!this.props.isOpen) {
      return (
        <div
          className="modaldisplay-closed"
          data-testid="modaldisplay-div-is-not-open"
        ></div>
      );
    }
    /*
 #Event Handling
  passing a function to the onClick Event 
 */
    return (
      <div data-testid="modaldisplay-div-is-open">
        <Modal
          onRequestClose={this.onClose}
          isOpen={this.props.isOpen}
          ariaHideApp={false}
          className="mymodal"
          overlayClassName="myoverlay"
        >
          <h5 className="categories-title">{this.props.title}</h5>
          <div>{this.props.value}</div>

          <button
            data-testid="modaldisplay-modal-close-button"
            onClick={this.onClose}
          >
            Close
          </button>
        </Modal>
      </div>
    );
  }
}

export default ModalDisplay;
