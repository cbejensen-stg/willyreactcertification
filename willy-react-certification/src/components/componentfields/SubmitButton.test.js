import React from "react";
import ReactDOM from "react-dom";
import { act } from "react-dom/test-utils";
import SubmitButton from "./SubmitButton";
import { screen } from "@testing-library/dom";

let container;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  document.body.removeChild(container);
  container = null;
});

it("can render and update a counter", () => {
  const text = "Login";
  const isDisabled = true;
  const onClickEvent = jest.fn();
  const buttonclassName = "buttonclassName";
  act(() => {
    ReactDOM.render(
      <SubmitButton
        text={text}
        buttonclassName={buttonclassName}
        disabled={isDisabled}
        onClick={onClickEvent}
      />,
      container
    );
  });
  const button = container.querySelector("button");

  expect(button.textContent).toBe(text);

  act(() => {
    button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
  });
});
