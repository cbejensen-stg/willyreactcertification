import React, { Component } from "react";

class InputField extends Component {
  render() {
    return (
      <div
        data-testid="inputField-div"
        className={
          this.props.divclassName ? this.props.divclassName : "inputField"
        }
      >
        <input
          data-testid={
            this.props.data_testid ? this.props.data_testid : "inputField-input"
          }
          className={
            this.props.inputclassName ? this.props.inputclassName : "input"
          }
          type={this.props.type}
          placeholder={this.props.placeholder}
          value={this.props.value}
          onChange={(event) => this.props.onChange(event.target.value)}
        />
      </div>
    );
  }
}

export default InputField;
