import React, { Component } from "react";
import { Link } from "react-router-dom";

class NavBar extends Component {
  render() {
    const navStyle = {
      color: "white",
    };

    return (
      <nav>
        <h3>Logo</h3>
        <ul className="nav-links">
          <Link style={navStyle} to="/">
            <li>Home</li>
          </Link>
          <Link style={navStyle} to="/categories">
            <li>Categories</li>
          </Link>
          <Link style={navStyle} to="/search">
            <li>Search</li>
          </Link>
          <Link style={navStyle} to="/jokes">
            <li>Jokes</li>
          </Link>
        </ul>
      </nav>
    );
  }
}

export default NavBar;
