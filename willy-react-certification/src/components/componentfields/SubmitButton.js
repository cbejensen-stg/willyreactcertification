import React, { Component } from "react";

class SubmitButton extends Component {
  count = 0;
  render() {
    this.count++;

    return (
      <>
        <button
          className={
            this.props.buttonclassName ? this.props.buttonclassName : "app-btn"
          }
          data-testid={this.props.data_testid ? this.props.data_testid : ""}
          disabled={this.props.disabled}
          onClick={() => this.props.onClick()}
        >
          {this.props.text}
        </button>
      </>
    );
  }
}

export default SubmitButton;
