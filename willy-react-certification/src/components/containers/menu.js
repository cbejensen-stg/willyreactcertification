import React, { Component } from "react";

class Menu extends Component {
  render() {
    let linksMarkup = this.props.links.map((link) => {
      return (
        <li key={link.id} className="menu-link">
          <a href={link.link}>{link.label}</a>
          <p>{link.description}</p>
        </li>
      );
    });
    return (
      <div>
        <menu>{linksMarkup}</menu>
      </div> 
    );
  }
}

export default Menu;
