import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";

import Menu from "./Menu";

let container = null;
let links = [];
beforeEach(() => {
  
  container = document.createElement("div");
  document.body.appendChild(container);
  links = [
    {
      id: 1,
      label: "Home",
      link: "home",
      description: "The main (landing) page && login page",
    },
    {
      id: 2,
      label: "Categories",
      link: "categories",
      description: "A page which will contain all joke categories",
    },
    {
      id: 3,
      label: "Search",
      link: "search",
      description: "A page to search for a joke by a search term",
    },
    {
      id: 4,
      label: "Jokes",
      link: "jokes",
      description: "A page the lists all viewed jokes",
    },
  ];
});

afterEach(() => {
  
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("Menu Testing", () => {
  it("check description is showing", () => {
    act(() => {
      render(<Menu links={links} />, container);
    });

    
    links.forEach((item) => {
      expect(container).toHaveTextContent(item.label);
      expect(container).toHaveTextContent(item.description);
    });
  });
});
