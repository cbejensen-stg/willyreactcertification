import React, { Component } from "react";
import ModalDisplay from "../componentfields/ModalDisplay";

class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      postTitle: "",
      postValue: "",
    };
  }

  SetModalValueOnClick(title, value) {
    this.setState({ postValue: value, postTitle: title, show: true });
  }

  onRequestClose = (e) => {
    this.setState((state) => ({
      show: !state.show,
    }));
  };
  render() {
    let tempId = 0;
    let tempResult;

    if (this.props.posts) {
      tempResult = this.props.posts.map((post) => {
        return (
          <li key={post.id} className="list-group-item">
            <a
              data-testid={"searchresult-ul-li-al-".concat(++tempId)}
              data-toggle="modal"
              href="/search#"
              onClick={() =>
                this.SetModalValueOnClick(post.categories[0], post.value)
              }
            >
              {post.value}
            </a>
          </li>
        );
      });
    }

    return (
      <div className="searchresult-div">
        <ModalDisplay
          onRequestClose={this.onRequestClose}
          isOpen={this.state.show}
          title={this.state.postTitle}
          value={this.state.postValue}
        />
        <ul
          data-testid="searchresult-ul"
          className="search-result list-group mb-4 search-list"
        >
          {tempResult}
        </ul>
      </div>
    );
  }
}

export default SearchResult;
