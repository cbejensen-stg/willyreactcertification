import reducer from "./categories.reducer";
import * as types from "../constants/ActionTypes";

import { categories, mockResponse } from "../../testing/testData";

describe("categories reducer", () => {
  let jokeId = 0;
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual({
      items: [],
      item: {
        categories: [],
        value: "",
      },
      Jokes: [],
      show: false,
      errors: [],
    });
  });

  it("should handle FETCH_CATEGORIES_SUCCESS", () => {
    expect(
      reducer([], {
        type: types.FETCH_CATEGORIES_SUCCESS,
        payload: categories,
      })
    ).toEqual({
      items: categories,
    });
  });

  it("should handle FETCH_CATEGORIES_FAILED", () => {
    const errorMessage = "categories Error";
    const expected = {
      errors: [errorMessage],
      items: [],
      item: {
        categories: [],
        value: "",
      },
      Jokes: [],
      show: false,
    };
    expect(
      reducer(undefined, {
        type: types.FETCH_CATEGORIES_FAILED,
        payload: errorMessage,
      })
    ).toEqual(expected);
  });

  it("should handle FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS", () => {
    ++jokeId;

    const expected = {
      errors: [],
      items: [],
      item: mockResponse.result[0],
      Jokes: [{ ...mockResponse.result[0], jokeId: jokeId }],
      show: true,
    };
    expect(
      reducer(undefined, {
        type: types.FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS,
        payload: mockResponse.result[0],
      })
    ).toEqual(expected);
  });

  it("should handle SHOWMODAL", () => {
    expect(
      reducer([], {
        type: types.SHOWMODAL,
        payload: true,
      })
    ).toEqual({
      show: true,
    });
  });

  it("should handle ADD_JOKE", () => {
    ++jokeId;

    const expected = {
      items: [],
      item: { categories: [], value: "" },
      Jokes: [{ ...mockResponse.result[0], jokeId: jokeId }],
      show: false,
      errors: [],
    };

    const actual = reducer(undefined, {
      type: types.ADD_JOKE,
      payload: mockResponse.result[0],
    });

    expect(actual).toEqual(expected);
  });
});
