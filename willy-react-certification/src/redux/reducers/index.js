import { combineReducers } from "redux";

import loggedReducer from "./isLogged";

import categoriesReducer from "./categories.reducer";

export default combineReducers({
  isLogged: loggedReducer,
  categories: categoriesReducer,
});
