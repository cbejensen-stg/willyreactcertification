import * as actions from "../constants/ActionTypes";

let jokeId = 0;
const initialState = {
  errors: [],
  items: [],
  item: {
    categories: [],
    value: "",
  },
  Jokes: [],
  show: false,
};

const categoriesReducer = (state = initialState, action) => {
  let temp = {};
  switch (action.type) {
    case actions.FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        items: action.payload,
      };

    case actions.FETCH_CATEGORIES_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.payload],
      };

    case actions.FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS:
      temp = { ...action.payload, jokeId: ++jokeId };
      return {
        ...state,
        item: action.payload,
        Jokes: [...state.Jokes, temp],
        show: true,
      };
    case actions.SHOWMODAL:
      return {
        ...state,
        show: action.payload,
      };
    case actions.ADD_JOKE:
      temp = { ...action.payload, jokeId: ++jokeId };
      return {
        ...state,
        Jokes: [...state.Jokes, temp],
      };

    default:
      return state;
  }
};

export default categoriesReducer;
