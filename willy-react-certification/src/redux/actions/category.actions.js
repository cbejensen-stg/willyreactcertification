import * as actions from "../constants/ActionTypes";

export function fetchCategoriesSuccess(payload) {
  return {
    type: actions.FETCH_CATEGORIES_SUCCESS,
    payload,
  };
}
export function fetchCategoriesFailed(ex) {
  return {
    type: actions.FETCH_CATEGORIES_FAILED,
    ex,
  };
}

export function fetchRandomCategoriesSuccess(payload) {
  return {
    type: actions.FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS,
    payload,
  };
}

export function fetchRandomCategoriesFailed(ex) {
  return {
    type: actions.FETCH_RANDOM_JOKE_BY_CATEGORY_FAILED,
    ex,
  };
}
/*
 #Closure
  dispatch function is receive a result and will execute once this method is done being executed
 */
export const getCategories = () => (dispatch) => {
  return fetch("https://api.chucknorris.io/jokes/categories")
    .then((response) => response.json())
    .then(
      (result) => {
        dispatch(fetchCategoriesSuccess(result));
      },
      (error) => {
        dispatch(
          fetchCategoriesFailed("category.actions.getCategories.error " + error)
        );
      }
    );
};
/*
#Arrow function
now that was interesting to learn...
*/
export const getRandomByCategory = (category) => (dispatch) => {
  fetch("https://api.chucknorris.io/jokes/random?category=" + category)
    .then((response) => response.json())
    .then(
      (result) => {
        dispatch(fetchRandomCategoriesSuccess(result));
      },
      (error) => {
        fetchRandomCategoriesFailed(
          "categories.actions.getRandomByCategory.error" + error
        );
      }
    );
};

export const showModal = (value) => {
  return { type: actions.SHOWMODAL, payload: value };
};
