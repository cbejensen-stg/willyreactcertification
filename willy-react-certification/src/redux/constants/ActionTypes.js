export const actionDisplay = "action";
export const reducerDisplay = "reducer";

export const SIGN_IN = "SIGN_IN";
export const SIGN_OUT = "SIGN_OUT";

export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS";
export const FETCH_CATEGORIES_FAILED = "FETCH_CATEGORIES_FAILED";
export const FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS =
  "FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS";
export const FETCH_RANDOM_JOKE_BY_CATEGORY_FAILED =
  "FETCH_RANDOM_JOKE_BY_CATEGORY_FAILED";
export const SHOWMODAL = "SHOWMODAL";
export const ADD_JOKE = "ADD_JOKE";
