import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { Provider } from "react-redux";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";
import store from "../redux/store/store";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { login, getRandomByCategory } from "../redux/actions";
import { mockResponse } from "../testing/testData";
import Search from "./Search";
import SearchResult from "../components/containers/SearchResult";
let container = null;

const history = createMemoryHistory();
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("Joke Testing", () => {
  it("Redirected", () => {
    store.dispatch(login(false));

    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Search />
          </Router>
        </Provider>,
        container
      );
    });

    expect(screen.getByTestId("redirect-to-home-page")).toBeInTheDocument();
  });

  it("Page Loaded", () => {
    store.dispatch(login(true));

    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Search />
          </Router>
        </Provider>,
        container
      );
    });

    expect(container.textContent).toContain("Search");
  });

  it("input field updated, Search Button Pushed, Fetch Called", (done) => {
    store.dispatch(login(true));
    const mockJsonPromise = Promise.resolve(mockResponse);
    const mockFetchPromise = Promise.resolve({ json: () => mockJsonPromise });

    jest.spyOn(global, "fetch").mockImplementation(() => mockFetchPromise);
    const searchTerm = "Testing";
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Search />
          </Router>
        </Provider>,
        container
      );
    });

    const inputField = screen.getByTestId("search-inputfield");
    const submitButton = screen.getByTestId("search-submitbutton");

    fireEvent.change(inputField, { target: { value: searchTerm } });
    fireEvent(
      submitButton,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    expect(global.fetch).toHaveBeenCalledTimes(1);
    expect(global.fetch).toHaveBeenCalledWith(
      "https://api.chucknorris.io/jokes/search?query=" + searchTerm
    );

    global.fetch.mockClear();
    done();
  });

  it("Search Result Showing", (done) => {
    store.dispatch(login(true));

    const mockJsonPromise = Promise.resolve(mockResponse);
    const mockFetchPromise = Promise.resolve({ json: () => mockJsonPromise });

    jest.spyOn(global, "fetch").mockImplementation(() => mockFetchPromise);
    const searchTerm = "Testing";
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Search />
          </Router>
        </Provider>,
        container
      );
    });

    const inputField = screen.getByTestId("search-inputfield");
    const submitButton = screen.getByTestId("search-submitbutton");

    fireEvent.change(inputField, { target: { value: searchTerm } });
    fireEvent(
      submitButton,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    expect(global.fetch).toHaveBeenCalledTimes(1);
    global.fetch.mockClear();
    done();

    const pagination = screen.getByTestId("custom-pagination-div");
  });

  it("Feth throw error", (done) => {
    store.dispatch(login(true));

    const mockJsonPromise = Promise.resolve(mockResponse);
    const mockFetchPromise = new Promise((reject, resolve) => {
      throw new error("ERROR");
    });

    jest.spyOn(global, "fetch").mockImplementation(() => mockFetchPromise);
    const searchTerm = "Testing";
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Search />
          </Router>
        </Provider>,
        container
      );
    });

    const inputField = screen.getByTestId("search-inputfield");
    const submitButton = screen.getByTestId("search-submitbutton");

    fireEvent.change(inputField, { target: { value: searchTerm } });
    fireEvent(
      submitButton,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    expect(global.fetch).toHaveBeenCalledTimes(1);

    global.fetch.mockClear();
    done();
  });
});
