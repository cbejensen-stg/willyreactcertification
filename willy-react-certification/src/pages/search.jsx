import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import InputField from "../components/componentfields/InputField";
import SubmitButton from "../components/componentfields/SubmitButton";
import CustomPagination from "../components/componentfields/CustomPagination";
import SearchResult from "../components/containers/SearchResult";
import { connect } from "react-redux";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "",
      Loading: false,
      show: false,
      currentPage: 1,
      postsPerPage: 50,
      indexOfLastPost: 0,
      indexOfFirstPost: 0,
      currentPosts: [],
      searchQuotes: {
        total: 0,
        result: [],
      },
      error: null,
    };
    this.setInputValue = this.setInputValue.bind(this);
    this.searchSubmitClick = this.searchSubmitClick.bind(this);
  }

  async searchSubmitClick(event) {
    await fetch(
      "https://api.chucknorris.io/jokes/search?query=" + this.state.searchTerm
    )
      .then((response) => response.json())
      .then(
        (data) => {
          this.setState({
            searchQuotes: data,
            loading: false,
          });
        },
        (error) => {
          this.setState({ error });
        }
      );
  }

  setInputValue(property, value) {
    value = value.trim();
    this.setState({
      [property]: value,
    });
  }

  render() {
    if (!this.props.isLogged) {
      return (
        <div data-testid="redirect-to-home-page">
          <Redirect to="/" />
        </div>
      );
    } else {
      const updateCurrentPage = (pageNumber) => {
        this.setState({ currentPage: pageNumber });
      };

      const indexOfLastPost = this.state.currentPage * this.state.postsPerPage;
      const indexOfFirstPost = indexOfLastPost - this.state.postsPerPage;

      let displayResult;
      let currentPosts;
      if (this.state.searchQuotes.result) {
        currentPosts = this.state.searchQuotes.result.slice(
          indexOfFirstPost,
          indexOfLastPost
        );
        /*
#Spread Operator
 i am putting all parameter within an object and pass that object into a component as a spread operator
*/
        const props = {
          posts: currentPosts,
          loading: this.state.loading,
          isOpen: this.state.show,
        };
        displayResult = (
          <div>
            <SearchResult {...props} />
          </div>
        );
      }

      return (
        <div>
          <div className="search">
            <InputField
              data_testid="search-inputfield"
              inputclassName="search-input"
              type="text"
              placeholder="Search..."
              value={this.state.searchTerm ? this.state.searchTerm : ""}
              onChange={(value) => this.setInputValue("searchTerm", value)}
            />
            <SubmitButton
              data_testid="search-submitbutton"
              buttonclassName="search-btn"
              text="Search"
              onClick={(event) => this.searchSubmitClick(event)}
            />
          </div>
          <CustomPagination
            postsPerPage={this.state.postsPerPage}
            totalPosts={this.state.searchQuotes.total}
            paginate={updateCurrentPage}
          />
          {displayResult}
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  isLogged: state.isLogged,
});

export default connect(mapStateToProps)(Search);
