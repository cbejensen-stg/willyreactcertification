import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { Provider } from "react-redux";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";
import store from "../redux/store/store";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { login, logout } from "../redux/actions";
import { getRandomByCategory } from "../redux/actions/category.actions";
import { mockCategories, mockItem } from "../testing/testData";
import Categories from "./Categories";

let container = null;

const history = createMemoryHistory();

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);

  const mockJsonPromise = Promise.resolve(mockCategories);
  const mockJsonPromiseItem = Promise.resolve(mockItem);
  const mockFetchPromise = Promise.resolve({ json: () => mockJsonPromise });
  const mockFetchPromiseItem = Promise.resolve({
    json: () => mockJsonPromiseItem,
  });

  const myMock = jest.fn();
  myMock.mockReturnValueOnce(mockCategories).mockReturnValueOnce(mockItem);

  let mockFetch = jest.fn(() => {});
  mockFetch
    .mockReturnValueOnce(mockFetchPromise)
    .mockReturnValueOnce(mockFetchPromiseItem);

  global.fetch = mockFetch;
  fetch.mockClear();
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("Categories Testing", () => {
  it("Redirected", () => {
    store.dispatch(login(false));

    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Categories />
          </Router>
        </Provider>,
        container
      );
    });

    expect(screen.getByTestId("redirect-to-home-page")).toBeInTheDocument();
  });

  it("globabl Fetch called", async () => {
    store.dispatch(login(true));

    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Categories />
          </Router>
        </Provider>,
        container
      );
    });

    mockCategories.forEach((item) => {
      expect(container).toHaveTextContent(item);
    });
    expect(await (await global.fetch()).json()).toEqual(mockCategories);
  });
});
