import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import JokeTable from "../components/containers/jokeTable";

class Jokes extends Component {
  render() {
    if (!this.props.isLogged) {
      return (
        <div data-testid="redirect-to-home-page">
          <Redirect to="/" />
        </div>
      );
    } else {
      return (
        <div>
          <h1>Jokes</h1>
          <JokeTable jokes={this.props.jokes} />
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  isLogged: state.isLogged,
  jokes: state.categories.Jokes,
});

export default connect(mapStateToProps)(Jokes);
