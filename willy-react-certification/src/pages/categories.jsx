import React, { Component } from "react";
import { Redirect } from "react-router-dom";

import SubmitButton from "../components/componentfields/SubmitButton";
import ModalDisplay from "../components/componentfields/ModalDisplay";

import "../components/componentfields/styles.css";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  getCategories,
  getRandomByCategory,
  showModal,
} from "../redux/actions/category.actions";

/*
  #Class Component
  #Lifecycle methods 
   ** constructor
   ** componentDidMount: GetCategories() immediatetly get called after the component get mounted
   ** render
*/
class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
    };
  }

  componentDidMount() {
    if (this.props.categories.items.length < 1) {
      this.props.getCategories();
    }
  }

  getCategoryQuoteOnClick(e) {
    this.props.getRandomByCategory(e.temp);
  }

  showModal = () => {
    this.props.showModal(!this.props.show);
  };

  render() {
    if (!this.props.isLogged) {
      return (
        <div data-testid="redirect-to-home-page">
          <Redirect to="/" />
        </div>
      );
    } else {
      let index = 0;
      let tempCategories = [];
      /*
#JSX
  tempCategories is a JSX react element
*/

      tempCategories = this.props.categories.items.map((temp) => {
        const testId = "categories-SubmitButton-result-" + ++index;
        return (
          <SubmitButton
            data_testid={testId}
            text={temp}
            key={index}
            onClick={(e) => {
              this.getCategoryQuoteOnClick({ temp });
            }}
          />
        );
      });
      let temp = [];
      index = 0;
      const tempItems = [...this.props.categories.items];
      tempItems.forEach((item) => {
        const testId = "categories-SubmitButton-result-" + ++index;
        temp.push(
          <SubmitButton
            data_testid={testId}
            text={temp}
            key={index}
            onClick={(e) => {
              this.getCategoryQuoteOnClick({ temp });
            }}
          />
        );
      });

      let tempModalDisplay = null;
      if (this.props.categories.item) {
        tempModalDisplay = (
          <ModalDisplay
            onRequestClose={this.showModal}
            isOpen={this.props.show}
            title={this.props.categories.item.categories[0]}
            value={this.props.categories.item.value}
          />
        );
      }
      /*
      #Rendering Elements
        the return code is a rendering elements. if there is more then 1 html tag, everything needed to be within a <div> tag
      */
      return (
        <div>
          <h1>Categories</h1>
          <div className="categories-column">
            {tempModalDisplay}
            <div className="category-btn-group">{tempCategories}</div>
          </div>
        </div>
      );
    }
  }
}
Categories.propTypes = {
  getCategories: PropTypes.func.isRequired,
  getRandomByCategory: PropTypes.func.isRequired,
  categories: PropTypes.object,

  showModal: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isLogged: state.isLogged,
  categories: {
    items: state.categories.items,
    item: state.categories.item,
  },
  show: state.categories.show,
});

export default connect(mapStateToProps, {
  getCategories,
  getRandomByCategory,
  showModal,
})(Categories);
