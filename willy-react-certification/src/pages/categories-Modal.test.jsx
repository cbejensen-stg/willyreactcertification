import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { Provider } from "react-redux";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";
import store from "../redux/store/store";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { login } from "../redux/actions";
import { mockCategories, mockItem } from "../testing/testData";
import Categories from "./Categories";
import * as types from "../redux/constants/ActionTypes";
/*
Blocked-scoped variable let
let variable can be reasigned
*/
let container = null;

const history = createMemoryHistory();

let mockFetch;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("Categories Testing - Get Categories", () => {
  beforeEach(() => {
    /*
    #Promises
    used here to mock the api call and return wanted value

    #Blocked-scoped variable const
    unreasignable variable
    */
    const mockJsonPromise = Promise.resolve(mockCategories);
    const mockFetchPromise = Promise.resolve({ json: () => mockJsonPromise });

    mockFetch = jest.fn(() => {});
    mockFetch.mockReturnValueOnce(mockFetchPromise);

    global.fetch = mockFetch;
    fetch.mockClear();
  });

  it("Fetch called", () => {
    store.dispatch(login(true));

    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Categories />
          </Router>
        </Provider>,
        container
      );
    });

    expect(mockFetch.mock.calls.length).toBe(1);
    expect(mockFetch.mock.calls[0][0]).toBe(
      "https://api.chucknorris.io/jokes/categories"
    );
  });
});

describe("Categories Testing - Get Random Joke by Category", () => {
  beforeEach(() => {
    const mockJsonPromise = Promise.resolve(mockCategories);
    const mockJsonPromiseItem = Promise.resolve(mockItem);
    const mockFetchPromise = Promise.resolve({ json: () => mockJsonPromise });
    const mockFetchPromiseItem = Promise.resolve({
      json: () => mockJsonPromiseItem,
    });

    const myMock = jest.fn();
    myMock.mockReturnValueOnce(mockCategories).mockReturnValueOnce(mockItem);

    mockFetch = jest.fn(() => {});
    mockFetch.mockReturnValueOnce(mockFetchPromiseItem);

    global.fetch = mockFetch;
    fetch.mockClear();
  });

  it("Fetch called", () => {
    store.dispatch(login(true));
    store.dispatch({
      type: types.FETCH_CATEGORIES_SUCCESS,
      payload: mockCategories,
    });
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Categories />
          </Router>
        </Provider>,
        container
      );
    });

    const button = screen.getByTestId("categories-SubmitButton-result-1");
    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    expect(mockFetch.mock.calls.length).toBe(1);

    expect(mockFetch.mock.calls[0][0]).toBe(
      "https://api.chucknorris.io/jokes/random?category=" + mockCategories[0]
    );
  });

  it("ShowModal ", () => {
    store.dispatch(login(true));
    store.dispatch({
      type: types.FETCH_CATEGORIES_SUCCESS,
      payload: mockCategories,
    });
    store.dispatch({
      type: types.FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS,
      payload: mockItem,
    });

    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Categories />
          </Router>
        </Provider>,
        container
      );
    });

    expect(screen.getByTestId("modaldisplay-div-is-open")).toBeInTheDocument();
  });
});
