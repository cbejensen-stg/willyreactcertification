import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { Provider } from "react-redux";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";
import store from "../redux/store/store";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { login, addJoke } from "../redux/actions";

import Jokes from "./Jokes";

let container = null;
let item = {
  categories: ["career"],
  id: "zk14uc6xr82d7ig9qhaymg",
  value:
    "Chuck Norris is actually the front man for Apple. He let's Steve Jobs run the show when he's on a mission. Chuck Norris is always on a mission.",
  jokeId: 1,
};
const history = createMemoryHistory();
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("Joke Testing", () => {
  it("Redirected", () => {
    store.dispatch(login(false));

    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Jokes />
          </Router>
        </Provider>,
        container
      );
    });

    expect(screen.getByTestId("redirect-to-home-page")).toBeInTheDocument();
  });

  it("Item is showing", () => {
    store.dispatch(login(true));
    store.dispatch(addJoke(item));
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Jokes />
          </Router>
        </Provider>,
        container
      );
    });

    expect(container.textContent).toContain(item.value);
    expect(container.textContent).toContain(item.categories[0]);
  });
});
