import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import LoginForm from "../components/auth/LoginForm";
import SubmitButton from "../components/componentfields/SubmitButton";
import LoadingSpinner from "../components/componentfields/loadingSpinner";
import Menu from "../components/containers/menu";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { login, logout } from "../redux/actions";
/*
#this keyword
 "this" to access or set component classes properties or functions
*/
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
    this.handleLoginAuth = this.handleLoginAuth.bind(this);
  }

  handleLoginAuth(data) {
    /*
    #State
    using state to deal with local variable to be set. in this case, this will make sure that the Loading component shows
    */

    this.setState({
      loading: true,
    });
    /*
#Arrow Functions
function runs after the time was up
*/
    setTimeout(() => {
      this.setState({
        loading: false,
      });

      /*
      #Props
      calling function using props
      */
      this.props.login(true);

      this.props.history.push("/search");
    }, 1500);
  }

  handleLogoutClick() {
    this.props.logout();
  }

  render() {
    if (this.props.isLogged) {
      let links = [
        {
          id: 1,
          description: "The main (landing) page && login page",
        },
        {
          id: 2,
          label: "Categories",
          link: "categories",
          description: "A page which will contain all joke categories",
        },
        {
          id: 3,
          label: "Search",
          link: "search",
          description: "A page to search for a joke by a search term",
        },
        {
          id: 4,
          label: "Jokes",
          link: "jokes",
          description: "A page the lists all viewed jokes",
        },
      ];

      return (
        <div className="app">
          <div className="container">
            <SubmitButton
              data_testid="home-logout-button"
              text="log out"
              disabled={false}
              onClick={() => this.handleLogoutClick()}
            />
            Welcome Home
            <Menu links={links} />
          </div>
        </div>
      );
    } else {
      return (
        <div className="app">
          <div className="container">
            <LoginForm
              handleLoginAuth={this.handleLoginAuth}
              loading={this.state.loading}
            />
            {!this.state.loading ? (
              ""
            ) : (
              <div className="container">
                <span>Loading, please Wait...</span>
                <LoadingSpinner />
              </div>
            )}
          </div>
        </div>
      );
    }
  }
}

Home.propTypes = {
  login: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  isLogged: state.isLogged,
});

export default withRouter(connect(mapStateToProps, { login, logout })(Home));
