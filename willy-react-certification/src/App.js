import React, { Component } from "react";
import "./App.css";
import NavBar from "./components/componentfields/navbar";
import Home from "./pages/home";
import Categories from "./pages/categories";
import Search from "./pages/search";
import Jokes from "./pages/jokes";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
/*
#Routing
basic routing usage with switch 
*/
class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <NavBar />
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/home" exact>
              <Home />
            </Route>
            <Route exact path="/categories">
              <Categories />
            </Route>
            <Route exact path="/Search">
              <Search />
            </Route>
            <Route exact path="/jokes">
              <Jokes />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
